		/*========================== Attendance =========================*/
		/*======================================================================================*/
DROP TRIGGER IF EXISTS tg_insert_attendance;
DELIMITER $$
CREATE TRIGGER tg_insert_attendance
AFTER INSERT ON `Attendance`
FOR EACH ROW
	BEGIN
		
		DECLARE user_role NVARCHAR(100);
        
        DECLARE total_lesson_in_subject TINYINT UNSIGNED;
        DECLARE total_lesson_in_class SMALLINT UNSIGNED;
        
		DECLARE total_attendance_in_subject TINYINT UNSIGNED;
        DECLARE total_attendance_in_class SMALLINT UNSIGNED;
        
        DECLARE min_date_subject DATE;
        DECLARE max_date_subject DATE;
        
        DECLARE min_date_class DATE;
        DECLARE max_date_class DATE;
        
        -- get user role
        SELECT 	`role` INTO user_role
        FROM	`User`
        WHERE	id = NEW.user_id;
        
		-- get total lesson in subject
        SELECT 	COUNT(1) INTO total_lesson_in_subject
        FROM	Lesson
        WHERE	`type` = 'Required' AND subject_id = NEW.subject_id;

		-- get total lesson in class
        SELECT 	COUNT(1) INTO total_lesson_in_class
        FROM	Course_Subject cs
        JOIN	Class c ON cs.course_id = c.course_id
        JOIN	Lesson l ON l.subject_id = cs.subject_id
        WHERE	c.id = NEW.class_id;
        
        -- get total attendance in subject
		SELECT 	COUNT(1) INTO total_attendance_in_subject
        FROM	Class_Attendance ca
        WHERE	ca.class_id = NEW.class_id AND ca.subject_id = NEW.subject_id;
        
		-- get total attendance in class
		SELECT 	COUNT(1) INTO total_attendance_in_class
        FROM	Class_Attendance
        WHERE	class_id = NEW.class_id;
        
        -- get min date subject, max date subject
        SELECT 	CAST(MIN(`date`) AS DATE), CAST(MAX(`date`) AS DATE) INTO min_date_subject, max_date_subject
		FROM	Class_Attendance
		WHERE	class_id = NEW.class_id AND subject_id = NEW.subject_id;
        
		-- get min date class, max date class
        SELECT 	CAST(MIN(`date`) AS DATE), CAST(MAX(`date`) AS DATE) INTO min_date_class, max_date_class
		FROM	Class_Attendance
		WHERE	class_id = NEW.class_id;

		-- mentor
        IF(user_role = 'Mentor') THEN
			-- mentor: update class table (-- update class thành active)
			UPDATE 	`Class`
			SET 	`status` = 'Active'
			WHERE 	id = NEW.class_id;
            
			-- update Class_Mentor thành active
			UPDATE 	`Class_Mentor`
			SET 	`status` = 'Active'
			WHERE 	class_id = NEW.class_id AND subject_id = NEW.subject_id;
            
            -- mentor: update class table (-- update lại start date)
			UPDATE 	`Class`
			SET 	start_date = min_date_class
			WHERE 	id = NEW.class_id;
            
            -- update Class_Mentor (update lại joined_date) 
			UPDATE 	`Class_Mentor`
			SET 	joined_date = min_date_subject
			WHERE 	class_id = NEW.class_id AND subject_id = NEW.subject_id;
            
			-- subject finished
			IF(total_attendance_in_subject = total_lesson_in_subject) THEN
				-- update Class_Mentor update lại finished và update lại end_date = ngày sau cùng
				UPDATE 	`Class_Mentor`
				SET 	`status` = 'Finished',
						end_date = max_date_subject
				WHERE 	class_id = NEW.class_id AND subject_id = NEW.subject_id;
			END IF;
			
			-- class finished
			IF(total_attendance_in_class = total_lesson_in_class) THEN
				-- update class update lại finished và update lại end_date = ngày sau cùng
				UPDATE 	`Class`
				SET 	`status` = 'Finished',
						end_date = max_date_class
				WHERE 	id = NEW.class_id;
			END IF;
            
		-- mentee
		ELSE
			-- update student table (update student thành active)
			UPDATE 	`Student`
			SET 	`status` = 'Active'
			WHERE 	id = NEW.user_id;
            
            -- update Class_Student thành active
			UPDATE 	`Class_Student`
			SET 	`status` = 1 -- 'Active'
			WHERE 	class_id = NEW.class_id AND student_id = NEW.user_id;
            
            -- update Class_Student (update lại joined_date)
			UPDATE 	`Class_Student`
			SET 	joined_date = min_date_class
			WHERE 	class_id = NEW.class_id AND mentee_id = NEW.user_id;
            
			-- class finished
			IF(total_attendance_in_class = total_lesson_in_class) THEN
				-- update class_student (update lại finished và update lại end_date = ngày sau cùng)
				UPDATE 	`Class_Student`
				SET 	`status` = 2, -- 'Finished',
						end_date = max_date_class
				WHERE 	class_id = NEW.class_id AND student_id = NEW.user_id;
                
                -- update student (update lại finished)
                UPDATE 	`Student`
				SET 	`status` = 'Finished'
				WHERE 	id = NEW.user_id;
			END IF;
		END IF;
	END$$
DELIMITER ;

DROP TRIGGER IF EXISTS tg_update_attendance;
DELIMITER $$
CREATE TRIGGER tg_update_attendance
AFTER UPDATE ON `Attendance`
FOR EACH ROW
	BEGIN
    
		DECLARE user_role NVARCHAR(100);
        
        DECLARE total_lesson_in_subject TINYINT UNSIGNED;
        DECLARE total_lesson_in_class SMALLINT UNSIGNED;
        
		DECLARE total_attendance_in_subject TINYINT UNSIGNED;
        DECLARE total_attendance_in_class SMALLINT UNSIGNED;
        
        DECLARE min_date_subject DATE;
        DECLARE max_date_subject DATE;
        
        DECLARE min_date_class DATE;
        DECLARE max_date_class DATE;
        
        -- get user role
        SELECT 	`role` INTO user_role
        FROM	`User`
        WHERE	id = NEW.user_id;
        
		-- get total lesson in subject
        SELECT 	COUNT(1) INTO total_lesson_in_subject
        FROM	Lesson
        WHERE	`type` = 'Required' AND subject_id = NEW.subject_id;

		-- get total lesson in class
        SELECT 	COUNT(1) INTO total_lesson_in_class
        FROM	Course_Subject cs
        JOIN	Class c ON cs.course_id = c.course_id
        JOIN	Lesson l ON l.subject_id = cs.subject_id
        WHERE	c.id = NEW.class_id;
        
        -- get total attendance in subject
		SELECT 	COUNT(1) INTO total_attendance_in_subject
        FROM	Class_Attendance ca
        WHERE	ca.class_id = NEW.class_id AND ca.subject_id = NEW.subject_id;
        
		-- get total attendance in class
		SELECT 	COUNT(1) INTO total_attendance_in_class
        FROM	Class_Attendance
        WHERE	class_id = NEW.class_id;
        
        -- get min date subject, max date subject
        SELECT 	CAST(MIN(`date`) AS DATE), CAST(MAX(`date`) AS DATE) INTO min_date_subject, max_date_subject
		FROM	Class_Attendance
		WHERE	class_id = NEW.class_id AND subject_id = NEW.subject_id;
        
		-- get min date class, max date class
        SELECT 	CAST(MIN(`date`) AS DATE), CAST(MAX(`date`) AS DATE) INTO min_date_class, max_date_class
		FROM	Class_Attendance
		WHERE	class_id = NEW.class_id;
        
        IF(user_role = 'Mentor') THEN
			-- mentor: update class table (-- update lại start date)
			UPDATE 	`Class`
			SET 	start_date = min_date_class
			WHERE 	id = NEW.class_id;
            
            -- update Class_Mentor (update lại joined_date) 
			UPDATE 	`Class_Mentor`
			SET 	joined_date = min_date_subject
			WHERE 	class_id = NEW.class_id AND subject_id = NEW.subject_id;
        
			-- subject finished
			IF(total_attendance_in_subject = total_lesson_in_subject) THEN
				-- update Class_Mentor (update lại end_date = ngày sau cùng)
				UPDATE 	`Class_Mentor`
				SET 	end_date = max_date_subject
				WHERE 	class_id = NEW.class_id AND subject_id = NEW.subject_id;
			END IF;
            
            -- class finished
			IF(total_attendance_in_class = total_lesson_in_class) THEN
				-- update class table (update lại end_date = ngày sau cùng)
				UPDATE 	`Class`
				SET 	end_date = max_date_class
				WHERE 	id = NEW.class_id;
			END IF;
            
        -- mentee
		ELSE
			-- update Class_Student (update lại joined_date)
			UPDATE 	`Class_Student`
			SET 	joined_date = min_date_class
			WHERE 	class_id = NEW.class_id AND mentee_id = NEW.user_id;
            
			-- class finished
			IF(total_attendance_in_class = total_lesson_in_class) THEN
				-- update class_mentee (update lại end_date = ngày sau cùng)
				UPDATE 	`Class_Student`
				SET 	end_date = max_date_class
				WHERE 	class_id = NEW.class_id AND student_id = NEW.user_id;
			END IF;
        END IF;
	END$$
DELIMITER ;