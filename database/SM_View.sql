SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));
		/*========================== Subject =========================*/
		/*======================================================================================*/
CREATE OR REPLACE VIEW Total_Lesson_In_Subject AS (
    SELECT		row_number() over (order by s.id) id, s.id AS subject_id, COUNT(l.id) AS total_lesson
    FROM		`Subject` s
	LEFT JOIN  	Lesson l ON l.subject_id = s.id
    GROUP BY	s.id
);

		/*========================== COURSE =========================*/
		/*======================================================================================*/
CREATE OR REPLACE VIEW Total_Subject_Total_Lesson_In_Course AS (
    SELECT		row_number() over (order by cs.course_id) id, cs.course_id, COUNT(DISTINCT cs.subject_id) AS total_subject, COUNT(1) AS total_lesson
    FROM		`Course_Subject` cs
    JOIN  		`Lesson` l ON l.subject_id = cs.subject_id
    GROUP BY 	cs.course_id
);

CREATE OR REPLACE VIEW Suggest_Student_Code AS (
	WITH Code_For_Each_Code_Course AS (
		SELECT 		cr.id AS course_id, cr.`student_code`, CONCAT(cr.`student_code`, LPAD(COUNT(cs.class_id) + 1, 6, 0)) AS `suggest_code`
		FROM 		course cr
		LEFT JOIN 	class c	ON	cr.id = c.course_id
		LEFT JOIN 	class_student cs ON cs.class_id = c.id 
		GROUP BY	cr.`student_code`
    )
    SELECT		row_number() over (order by c.id) id, c.id AS `course_id`, cfecc.`suggest_code`
    FROM		Course c 
    LEFT JOIN  	Code_For_Each_Code_Course cfecc ON c.`student_code` = cfecc.`student_code`
);

CREATE OR REPLACE VIEW Suggest_Class_Code AS (
	WITH Code_For_Each_Code_Course AS (
		SELECT 		cr.id AS course_id, cr.`class_code`, CONCAT(cr.`class_code`, '-', LPAD(COUNT(c.id) + 1, 3, 0)) AS `suggest_code`
		FROM 		course cr
		LEFT JOIN 	class c	ON	cr.id = c.course_id
		GROUP BY	cr.`class_code`
    )
    SELECT		row_number() over (order by c.id) id, c.id AS `course_id`, cfecc.`suggest_code`
    FROM		Course c 
    LEFT JOIN  	Code_For_Each_Code_Course cfecc ON c.`class_code` = cfecc.`class_code`
);

CREATE OR REPLACE VIEW Active_Class_By_Course AS (
	SELECT 		row_number() over (order by c.id) id, c.course_id, c.id AS class_id 
	FROM 		Class c
	WHERE		c.`status` = "Not-started" OR c.`status` = "Active"
);

		/*========================== ATTENDANCE =========================*/
		/*======================================================================================*/
                
CREATE OR REPLACE VIEW Class_Attendance AS (
	SELECT 		row_number() over (order by a.user_id) id, a.user_id AS mentor_id, a.class_id, a.subject_id, a.lesson_id, a.`date`
	FROM 		Attendance a
    JOIN		`User` u ON a.user_id = u.id
	JOIN		Lesson l ON l.id = a.lesson_id
	WHERE		u.`role` = 'Mentor'
);

CREATE OR REPLACE VIEW Subject_Class_Progress AS (
	WITH subject_class AS (
			SELECT 		c.id AS class_id,  cs.subject_id, cs.ordinal, COUNT(1) as total_lesson
			FROM 		class c
			JOIN		course_subject cs 	ON	cs.course_id = c.course_id
			JOIN		lesson l ON l.subject_id = cs.subject_id	
			GROUP BY	c.id, cs.course_id, cs.subject_id
	),	finish_lesson AS (
			SELECT 		ca.class_id, ca.subject_id, COUNT(lesson_id) AS total_finished_lesson
			FROM 		class_attendance ca
			GROUP BY	ca.class_id, ca.subject_id
		)
		SELECT		row_number() over (order by sc.class_id) id, 
					sc.class_id, 
					sc.subject_id, 
					sc.ordinal, 
					sc.total_lesson, 
					if(fl.total_finished_lesson IS NULL, 0, fl.total_finished_lesson) AS total_finished_lesson
		FROM		subject_class sc 
		LEFT JOIN  	finish_lesson fl ON sc.`class_id` = fl.`class_id` AND sc.`subject_id` = fl.`subject_id`
);

		/*========================== CLASS =========================*/
		/*======================================================================================*/
CREATE OR REPLACE VIEW Unassigned_Mentor_In_Subject_By_Class AS (
	WITH compulsory_subject_by_course AS (
		SELECT 	c.id AS class_id, cbs.subject_id
		FROM	Course_Subject cbs
		JOIN	Class	c ON cbs.course_id = c.course_id
	),
	assigned_mentor_or_active_or_finish_subject_by_class AS (
		SELECT 	cm.class_id, cm.subject_id
		FROM	Class_Mentor cm
	)
	SELECT 		row_number() over (order by csbc.class_id) id, csbc.class_id, csbc.subject_id
	FROM		compulsory_subject_by_course csbc
	LEFT JOIN	assigned_mentor_or_active_or_finish_subject_by_class amoaofsbc	ON	csbc.class_id = amoaofsbc.class_id AND csbc.subject_id = amoaofsbc.subject_id
	WHERE		amoaofsbc.class_id IS NULL AND amoaofsbc.subject_id IS NULL
);

CREATE OR REPLACE VIEW Current_Lesson_Subject_Mentor_By_Class AS (
	WITH subject_in_class AS (
		SELECT 		DISTINCT ca.class_id, ca.subject_id, cs.ordinal
		FROM 		Class_Attendance ca
		JOIN		Class c ON c.id = ca.class_id
		JOIN		course_subject cs ON cs.course_id = c.course_id AND ca.subject_id = cs.subject_id
		WHERE 		c.`status` = 'Active'
    ),	
    max_subject_ordinal AS (
		SELECT 		class_id, MAX(ordinal) AS max_ordinal
		FROM 		subject_in_class
        GROUP BY	class_id
    ),
	current_subject_in_class AS (
		SELECT 		sic.class_id, sic.subject_id AS subject_id
		FROM 		subject_in_class sic
        JOIN		max_subject_ordinal mso ON sic.class_id = mso.class_id AND sic.ordinal = mso.max_ordinal
	),
	current_lesson_subject_in_class AS (
		SELECT 		csic.class_id, csic.subject_id, MAX(ca.lesson_id) AS lesson_id
		FROM 		current_subject_in_class csic
		JOIN		Class_Attendance ca ON csic.class_id = ca.class_id AND csic.subject_id = ca.subject_id
		GROUP BY	ca.class_id
	)
	SELECT 		row_number() over (order by clsic.class_id) id, clsic.*, cm.mentor_id
	FROM		current_lesson_subject_in_class clsic
	LEFT JOIN	Class_Mentor cm ON clsic.class_id = cm.class_id AND clsic.subject_id = cm.subject_id
);

CREATE OR REPLACE VIEW Student_Rate_By_Class AS (
	WITH Total_Student AS (
		SELECT 		class_id, COUNT(student_id) AS student_amount
		FROM 		Class_Student
		GROUP BY	class_id
	),
    Total_Not_Started_Student AS (
		SELECT 		class_id, COUNT(student_id) AS student_amount
		FROM 		Class_Student
		WHERE		`status` = 0
		GROUP BY	class_id
	),
	Total_Active_Student AS (
		SELECT 		class_id, COUNT(student_id) AS student_amount
		FROM 		Class_Student
		WHERE		`status` = 1
		GROUP BY	class_id
	), 
	Total_Finished_Student AS (
		SELECT 		class_id, COUNT(student_id) AS student_amount
		FROM 		Class_Student
		WHERE		`status` = 2
		GROUP BY	class_id
	), 
	Total_Move_to_Student AS (
		SELECT 		class_id, COUNT(student_id) AS student_amount
		FROM 		Class_Student
		WHERE		`status` = 3
		GROUP BY	class_id
	), 
	Total_Deferred_Student AS (
		SELECT 		class_id, COUNT(student_id) AS student_amount
		FROM 		Class_Student
		WHERE		`status` = 4
		GROUP BY	class_id
	), 
	Total_Dropout_Student AS (
		SELECT 		class_id, COUNT(student_id) AS student_amount
		FROM 		Class_Student
		WHERE		`status` = 5
		GROUP BY	class_id
	)
	SELECT 		row_number() over (order by c.id) id, 
				c.id AS class_id,
                if(ts.student_amount IS NULL, 0, ts.student_amount) AS student_total,
                
                if(tnss.student_amount IS NULL, 0, tnss.student_amount) AS not_started_student_total,
                if(tas.student_amount IS NULL, 0, tas.student_amount) AS active_student_total,
                if(tfs.student_amount IS NULL, 0, tfs.student_amount) AS finished_student_total,
                if(tmts.student_amount IS NULL, 0, tmts.student_amount) AS move_to_student_total,
                if(tds.student_amount IS NULL, 0, tds.student_amount) AS dropout_student_total,
                if(tdes.student_amount IS NULL, 0, tdes.student_amount) AS deferred_student_total,
                
                if(tnss.student_amount IS NULL, 0, tnss.student_amount / ts.student_amount * 100) AS not_started_rate,
                if(tas.student_amount IS NULL, 0, tas.student_amount / ts.student_amount * 100) AS active_rate,
                if(tfs.student_amount IS NULL, 0, tfs.student_amount / ts.student_amount * 100) AS finished_rate,
                if(tmts.student_amount IS NULL, 0, tmts.student_amount / ts.student_amount * 100) AS move_to_rate,
                if(tds.student_amount IS NULL, 0, tds.student_amount / ts.student_amount * 100) AS dropout_rate,
                if(tdes.student_amount IS NULL, 0,tdes.student_amount / ts.student_amount * 100) AS deferred_rate
	FROM		Class c
    LEFT JOIN	Total_Student ts	ON ts.class_id = c.id
    LEFT JOIN 	Total_Not_Started_Student tnss ON ts.class_id = tnss.class_id
    LEFT JOIN 	Total_Active_Student tas ON ts.class_id = tas.class_id
    LEFT JOIN 	Total_Finished_Student tfs ON ts.class_id = tfs.class_id
	LEFT JOIN 	Total_Move_to_Student tmts ON ts.class_id = tmts.class_id
	LEFT JOIN	Total_Dropout_Student tds ON ts.class_id = tds.class_id
    LEFT JOIN 	Total_Deferred_Student tdes ON ts.class_id = tdes.class_id
);

		/*========================== MENTOR =========================*/
		/*======================================================================================*/
CREATE OR REPLACE VIEW Current_Lesson_By_Subject_Class_Mentor AS (
	WITH subject_class_mentor_active_or_finished AS (
		SELECT 		m.id as mentor_id, cm.class_id, cm.subject_id, cs.ordinal
		FROM 		mentor m
		LEFT JOIN 	class_mentor cm ON cm.mentor_id = m.id
		LEFT JOIN	class c ON cm.class_id = c.id
		LEFT JOIN	course_subject cs ON cs.course_id = c.course_id AND cm.subject_id = cs.subject_id
		WHERE 		cm.`status` IN ('Active', 'Finished') AND c.`status` = 'Active'
	),
	max_subject_ordinal AS (
		SELECT 		class_id, MAX(ordinal) AS max_ordinal
		FROM 		subject_class_mentor_active_or_finished
        GROUP BY	class_id
    ),
    current_subject_class_mentor AS(
		SELECT	scmaof.mentor_id, scmaof.class_id, scmaof.subject_id 
        FROM	subject_class_mentor_active_or_finished scmaof
		JOIN	max_subject_ordinal mso ON scmaof.class_id = mso.class_id AND scmaof.ordinal = mso.max_ordinal 
	),
	current_lesson_subject_class_mentor AS (
		SELECT 		cscm.mentor_id, cscm.class_id, cscm.subject_id, ca.lesson_id
		FROM 		current_subject_class_mentor cscm
		LEFT JOIN 	Class_Attendance ca ON ca.class_id = cscm.class_id AND ca.subject_id = cscm.subject_id
	)
	SELECT 		row_number() over (order by mentor_id) id, mentor_id, class_id, subject_id, MAX(lesson_id) AS current_lesson_id
	FROM 		current_lesson_subject_class_mentor
	GROUP BY 	class_id, subject_id
);

CREATE OR REPLACE VIEW Total_Hour_In_This_Month_By_Class_Mentor AS (
	SELECT 		row_number() over (order by a.user_id) id, a.user_id AS mentor_id, a.class_id, (COUNT(1) * cr.`hour`) AS hour_in_month
	FROM 		Attendance a
	JOIN		`Class` c ON c.id = a.class_id
    JOIN		`Course` cr ON cr.id = c.course_id
	WHERE 		MONTH(a.`date`) = MONTH(NOW()) 
				AND YEAR(a.`date`) = YEAR(NOW())
	GROUP BY 	a.user_id, a.class_id
);

CREATE OR REPLACE VIEW Total_Hour_In_This_Month_By_Mentor AS (
	SELECT 		row_number() over (order by ca.mentor_id) id, ca.mentor_id, (COUNT(1) * cr.`hour`) AS hour_in_month
	FROM 		class_attendance ca
	JOIN		`Class` c ON c.id = ca.class_id
    JOIN		`Course` cr ON cr.id = c.course_id
	WHERE 			MONTH(ca.`date`) = MONTH(NOW()) 
				AND YEAR(ca.`date`) = YEAR(NOW())
	GROUP BY 	ca.mentor_id
);  
    
		/*========================== MENTEE =========================*/
		/*======================================================================================*/
CREATE OR REPLACE VIEW Student_Full_Paid_Tuition AS (
	WITH student_payable_tuition AS (
		SELECT 	scp.student_id, scp.course_id, scp.payable_tuition 
		FROM	Student s
		JOIN 	student_course_payment scp ON s.id = scp.student_id
    ),
    student_paied_tuition AS (
		SELECT  sp.student_id, sp.course_id, SUM(sp.tuition) AS paied_tuition_total
		FROM	Student s
		JOIN	student_payment sp ON s.id = sp.student_id
		GROUP BY sp.student_id, sp.course_id
    )
    SELECT 	row_number() over (order by spt1.student_id) id, spt1.student_id, spt2.course_id, IF(spt2.paied_tuition_total >= spt1.payable_tuition, true, false) AS is_full_paied_tuition
    FROM 	student_payable_tuition spt1
    JOIN	student_paied_tuition spt2 ON spt1.student_id = spt2.student_id AND spt1.course_id = spt2.course_id
);


