		/*========================== CREATE DATABASE student management SYSTEM =========================*/
		/*======================================================================================*/
                           
DROP DATABASE IF EXISTS SM;
CREATE DATABASE SM;
USE SM;

		/*============================== CREATE TABLE ==========================================*/
		/*======================================================================================*/

-- Create table: User
DROP TABLE IF EXISTS `User`;
CREATE TABLE `User`(
	id 						INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    username				NVARCHAR(100) UNIQUE KEY NOT NULL,
    `password` 				VARCHAR(800) NOT NULL,
    first_name				NVARCHAR(100) NOT NULL,
    last_name				NVARCHAR(100) NOT NULL,
	gender					VARCHAR(100) NOT NULL,
    created_date			DATETIME DEFAULT NOW(),
    modified_date			DATETIME DEFAULT NOW(),	
    `role`					NVARCHAR(100) DEFAULT 'Student',  -- Mentor, Student, Admin
    editor_id				INT UNSIGNED,
    creator_id				INT UNSIGNED NOT NULL,
    
    FOREIGN KEY (creator_id) REFERENCES `User`(id),
    FOREIGN KEY (editor_id) REFERENCES `User`(id)
);

-- Create table: Course
DROP TABLE IF EXISTS `Course`;
CREATE TABLE `Course`(
	id 						INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    `class_code`			NVARCHAR(100) NOT NULL,
    `student_code`			NVARCHAR(100) NOT NULL,
    `name` 					NVARCHAR(100) NOT NULL UNIQUE KEY,
    `standard_tuition`		INT UNSIGNED NOT NULL,
    `hour`					FLOAT DEFAULT 3, -- 2h 3h
    created_date			DATETIME DEFAULT NOW(),
    modified_date			DATETIME DEFAULT NOW(),	
    note					NVARCHAR(500),
    editor_id				INT UNSIGNED,
    creator_id				INT UNSIGNED NOT NULL,
    
    FOREIGN KEY (creator_id) REFERENCES `User`(id),
    FOREIGN KEY (editor_id) REFERENCES `User`(id)
);

-- Create table: Course_Payment
DROP TABLE IF EXISTS `Course_Payment`;
CREATE TABLE `Course_Payment`(
	id 						INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    `type`					INT UNSIGNED NOT NULL,
    `payable_tuition`		INT UNSIGNED NOT NULL,
    course_id				INT UNSIGNED NOT NULL,
    
    FOREIGN KEY (course_id) REFERENCES `Course` (id)
);

-- Create table: Subject
DROP TABLE IF EXISTS `Subject`;
CREATE TABLE `Subject` (
    id 						INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    `name`					NVARCHAR(100) NOT NULL, -- sql, java core, frontend basic, java advance,reactjs, android, mock project, tester
    `code`					NVARCHAR(100) NOT NULL UNIQUE KEY,
    `version`				NVARCHAR(100),
    created_date			DATETIME DEFAULT NOW(),
    modified_date			DATETIME DEFAULT NOW(),
    note					NVARCHAR(500),
    editor_id				INT UNSIGNED,
    creator_id				INT UNSIGNED NOT NULL,
    
    FOREIGN KEY (creator_id) REFERENCES `User`(id),
    FOREIGN KEY (editor_id) REFERENCES `User`(id)
);

-- Create table: Course & Subject
DROP TABLE IF EXISTS Course_Subject;
CREATE TABLE Course_Subject(
	course_id 				INT UNSIGNED NOT NULL,				
	subject_id				INT UNSIGNED NOT NULL,
    ordinal					INT	UNSIGNED NOT NULL,
    PRIMARY KEY(course_id, subject_id),
	FOREIGN KEY (course_id) REFERENCES Course (id),
    FOREIGN KEY (subject_id) REFERENCES `Subject` (id)
    
);

-- Create table: Lesson
DROP TABLE IF EXISTS Lesson;
CREATE TABLE Lesson (
	id 					INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    `name`				NVARCHAR(100) NOT NULL,
    `type`				NVARCHAR(100) NOT NULL DEFAULT 'Required',
    subject_id 			INT UNSIGNED NOT NULL,
    ordinal				INT	UNSIGNED NOT NULL,
    created_date			DATETIME DEFAULT NOW(),
    modified_date			DATETIME DEFAULT NOW(),
    editor_id				INT UNSIGNED,
    creator_id				INT UNSIGNED NOT NULL,
    
    FOREIGN KEY (creator_id) REFERENCES `User`(id),
    FOREIGN KEY (editor_id) REFERENCES `User`(id),
	FOREIGN KEY (subject_id) REFERENCES `Subject` (id)
);

-- Create table: Lesson_Schedule
DROP TABLE IF EXISTS Lesson_Schedule;
CREATE TABLE Lesson_Schedule (
	id 					INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    `content`			NVARCHAR(200) NOT NULL,	-- document/ extra document
    `teaching_method`	NVARCHAR(100) NOT NULL,	-- Concept/Lecture, Assignment/Lab, Guides/Review, Exam
    `duration`			FLOAT NOT NULL,
    lesson_id 			INT UNSIGNED NOT NULL,
    
	FOREIGN KEY (lesson_id) REFERENCES `Lesson` (id)
);


-- Create table: Class
DROP TABLE IF EXISTS `Class`;
CREATE TABLE `Class`(
	id 						INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    course_id				INT UNSIGNED NOT NULL,
    `name` 					NVARCHAR(100) NOT NULL UNIQUE KEY,
    `code`					NVARCHAR(100) NOT NULL UNIQUE KEY,
    `type`					NVARCHAR(100) NOT NULL, -- Developer
    address					NVARCHAR(100) NOT NULL,
    facebook_link			NVARCHAR(100) NOT NULL,
    group_chat				NVARCHAR(100) NOT NULL,
    start_date				DATE NOT NULL,
    expected_end_date		DATE NOT NULL,
    end_date				DATE,
    `status`				ENUM('Not-started', 'Active', 'Merge', 'Finished') NOT NULL DEFAULT 'Not-started', -- Not-started, Active, Merge, Finished
	created_date			DATETIME DEFAULT NOW(),
    modified_date			DATETIME DEFAULT NOW(),
    note					NVARCHAR(500),
    editor_id				INT UNSIGNED,
    creator_id				INT UNSIGNED NOT NULL,
    
    FOREIGN KEY (creator_id) REFERENCES `User`(id),
    FOREIGN KEY (editor_id) REFERENCES `User`(id),
    FOREIGN KEY (course_id) REFERENCES `Course` (id)
);

-- Create table: Class_Schedule
DROP TABLE IF EXISTS Class_Schedule;
CREATE TABLE Class_Schedule (
	id 					INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    `name`				NVARCHAR(100) NOT NULL UNIQUE
);

-- Create table: Class_Class_Schedule
DROP TABLE IF EXISTS Class_Class_Schedule;
CREATE TABLE Class_Class_Schedule (
    `class_schedule_id`	INT UNSIGNED NOT NULL,
	class_id 			INT UNSIGNED NOT NULL,
    PRIMARY KEY(class_schedule_id, class_id),
    FOREIGN KEY (class_schedule_id) REFERENCES `Class_Schedule` (id),
	FOREIGN KEY (class_id) REFERENCES `Class` (id)
);


-- Create table: User_Detail
DROP TABLE IF EXISTS `User_Detail`;
CREATE TABLE `User_Detail`(
	user_id 					INT UNSIGNED PRIMARY KEY ,
    school						NVARCHAR(100),
    phone_number				NVARCHAR(100) UNIQUE KEY NOT NULL,
    email						NVARCHAR(100) UNIQUE KEY NOT NULL,
    date_of_birth				DATE NOT NULL,
    facebook_link				NVARCHAR(100) NOT NULL,
    current_address				NVARCHAR(100) ,
    home_town					NVARCHAR(100) ,
	current_working_location	NVARCHAR(100) ,
    identify_number				NVARCHAR(100) UNIQUE KEY,
    identify_date				NVARCHAR(100),
    identify_place				NVARCHAR(100),
    permanent_address			NVARCHAR(100),
    bank_name					NVARCHAR(100),
    account_number				NVARCHAR(100),
    bank_account_name			NVARCHAR(100),
    tax_number					NVARCHAR(100),
    tax_address					NVARCHAR(100),
    note						NVARCHAR(500),
    
    FOREIGN KEY (user_id) REFERENCES `User` (id)
);

-- Create table: Admin
DROP TABLE IF EXISTS `Admin`;
CREATE TABLE `Admin`(
	id 						INT UNSIGNED NOT NULL,
    
	FOREIGN KEY (id) REFERENCES `User` (id)
);


-- Create table: Student
DROP TABLE IF EXISTS Student;
CREATE TABLE Student(
	 id 						INT UNSIGNED PRIMARY KEY,
    `code` 						NVARCHAR(100) NOT NULL UNIQUE KEY,
    `status`					NVARCHAR(100) DEFAULT 'Not-started', -- Not-started, Active, Pending, Deferred, Drop-out, Finished
    
    FOREIGN KEY (id) REFERENCES `User` (id)
);

-- Create table: Student & Course_payment
-- luu student chon loai thanh toan nao
DROP TABLE IF EXISTS Student_Course_Payment;
CREATE TABLE Student_Course_Payment(
    `student_id` 				INT UNSIGNED NOT NULL,
    `course_id` 				INT UNSIGNED NOT NULL,
    `payment_type_id`			INT UNSIGNED NOT NULL,
    `payable_tuition`			INT UNSIGNED NOT NULL,
	created_date			DATETIME DEFAULT NOW(),
    modified_date			DATETIME DEFAULT NOW(),		
    PRIMARY KEY(student_id, course_id),
    FOREIGN KEY (`student_id`) REFERENCES `User` (id),
    FOREIGN KEY (`course_id`) REFERENCES `Course` (id),
    FOREIGN KEY (`payment_type_id`) REFERENCES `Course_Payment` (`id`)
);

-- Create table: Student_Payment
DROP TABLE IF EXISTS `Student_Payment`;
CREATE TABLE `Student_Payment`(
	`id` 					INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    student_id 				INT UNSIGNED NOT NULL,
    `course_id` 			INT UNSIGNED NOT NULL,
    `tuition`				INT UNSIGNED NOT NULL,
    created_date			DATETIME DEFAULT NOW(),
    modified_date			DATETIME DEFAULT NOW(),
    
    FOREIGN KEY (student_id) REFERENCES `Student` (id),
    FOREIGN KEY (`course_id`) REFERENCES `Course` (id)
);

-- Create table: Class & Student
DROP TABLE IF EXISTS Class_Student;
CREATE TABLE Class_Student(
	class_id 				INT UNSIGNED NOT NULL,
    student_id 				INT UNSIGNED NOT NULL,
    joined_date				DATETIME DEFAULT NOW(),
    end_date				DATETIME,
    `status`				INT UNSIGNED DEFAULT 0, -- 0: Not-started, 1: Active, 2: Finished, 3: Move-to, 4: Deferred, 5: Drop-out
    note					NVARCHAR(500),
    
    FOREIGN KEY (class_id) REFERENCES `Class` (id),
	FOREIGN KEY (student_id) REFERENCES Student (id),
    PRIMARY KEY(class_id, student_id)
);

-- Create table: Mentor
DROP TABLE IF EXISTS Mentor;
CREATE TABLE Mentor (
	id 						INT UNSIGNED NOT NULL PRIMARY KEY,				
	`type`					NVARCHAR(100) DEFAULT 'Internal',
    `status`				NVARCHAR(100) DEFAULT 'Active',
    
    FOREIGN KEY (id) REFERENCES `User` (id)
);

-- Create table: Mentor & Subject
DROP TABLE IF EXISTS Mentor_Subject;
CREATE TABLE Mentor_Subject(
	mentor_id 				INT UNSIGNED NOT NULL,				
	subject_id				INT UNSIGNED NOT NULL,
    
	FOREIGN KEY (mentor_id) REFERENCES Mentor (id),
    FOREIGN KEY (subject_id) REFERENCES `Subject` (id),
    PRIMARY KEY(mentor_id, subject_id)
);

-- Create table: Mentor & Salary
DROP TABLE IF EXISTS Mentor_Salary ;
CREATE TABLE Mentor_Salary(
	id						INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	mentor_id 				INT UNSIGNED ,				
	hourly_salary			INT UNSIGNED DEFAULT 170000,	-- 170000/200000/300000,...
    from_date				DATE NOT NULL,
  
    FOREIGN KEY (mentor_id) REFERENCES Mentor (id)
);

-- Create table: Class & Mentor
DROP TABLE IF EXISTS Class_Mentor;
CREATE TABLE Class_Mentor(
	class_id 				INT UNSIGNED NOT NULL,
    mentor_id 				INT UNSIGNED NOT NULL,
    subject_id				INT UNSIGNED NOT NULL,
    joined_date				DATE,
    end_date				DATE,
    `status`				NVARCHAR(100) DEFAULT 'Not-Started',	-- Not-Started, Active, Finished
    note					NVARCHAR(500),
    
	FOREIGN KEY (class_id) REFERENCES `Class` (id),
	FOREIGN KEY (mentor_id) REFERENCES Mentor (id),
	FOREIGN KEY (subject_id) REFERENCES `Subject` (id),
    PRIMARY KEY(class_id, subject_id)
);

-- Create table: Attendance
DROP TABLE IF EXISTS Attendance;
CREATE TABLE Attendance (
	id 						INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    user_id 				INT UNSIGNED NOT NULL, 		-- mentor & mentee
    class_id				INT UNSIGNED NOT NULL,
	subject_id				INT UNSIGNED NOT NULL,
	lesson_id				INT UNSIGNED NOT NULL,
    `type`					NVARCHAR(100) NOT NULL, 	-- P (Có mặt) A(Vắng mặt)
    `date`					DATETIME NOT NULL,		-- datetime
    
    FOREIGN KEY (user_id) REFERENCES `User` (id),
    FOREIGN KEY (class_id) REFERENCES `Class` (id),
	FOREIGN KEY (subject_id) REFERENCES `Subject` (id),
    FOREIGN KEY (lesson_id) REFERENCES `Lesson` (id)
);

-- Create table: Tuition
DROP TABLE IF EXISTS Tuition;
CREATE TABLE Tuition(
	id							INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	standard_tuition_total 		BIGINT UNSIGNED NOT NULL,				
	actual_tuition_total		BIGINT UNSIGNED NOT NULL
);

-- Create table: Token
DROP TABLE IF EXISTS 	`Token`;
CREATE TABLE IF NOT EXISTS `Token` ( 	
	id 				INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    `type`			VARCHAR(100) NOT NULL, -- ResetPassword, RefreshToken
	`token`	 		VARCHAR(100) NOT NULL UNIQUE,
	`user_id` 		INT UNSIGNED NOT NULL,
	`expiry_date` 	DATETIME NOT NULL,
    
    FOREIGN KEY (user_id) REFERENCES `User` (id)
);