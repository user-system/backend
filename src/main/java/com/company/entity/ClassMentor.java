package com.company.entity;

import java.io.Serializable;
import java.util.Date;

import org.hibernate.annotations.DynamicInsert;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.SuperBuilder;

@Entity
@Table(name = "class_mentor")
@Data
@SuperBuilder
@DynamicInsert
@NoArgsConstructor
public class ClassMentor implements Serializable {
	private static final long serialVersionUID = 1L;
	@EmbeddedId
	@NonNull
	private ClassMentorPK id;

	@ManyToOne
	@JoinColumn(name = "class_id", nullable = false,insertable=false, updatable=false)
	private Clazz clazz;

	@ManyToOne
	@JoinColumn(name = "mentor_id", nullable = false)
	private Mentor mentor;

	@ManyToOne
	@JoinColumn(name = "subject_id", nullable = false,insertable=false, updatable=false)
	private Subject subject;

	@Temporal(TemporalType.DATE)
	@Column(name = "joined_date")
	private Date joinedDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "end_date")
	private Date endDate;

	@Column(name = "status", length = 100, nullable = false)
	private String status;

	@Column(name = "note", length = 500)
	private String note;

	@Data
	@NoArgsConstructor
	@Embeddable
	@RequiredArgsConstructor
	public static class ClassMentorPK implements Serializable {

		private static final long serialVersionUID = 1L;

		@Column(name = "subject_id")
		@NonNull
		private Integer subjectId;

		@Column(name = "class_id")
		@NonNull
		private Integer classId;

	}
}
