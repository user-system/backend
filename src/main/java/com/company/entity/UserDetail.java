package com.company.entity;

import java.util.Date;

import org.hibernate.annotations.DynamicInsert;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.PrimaryKeyJoinColumn;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import lombok.experimental.SuperBuilder;

@Entity
@Table(name = "user_detail")
@PrimaryKeyJoinColumn(name = "user_id")
@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@DynamicInsert
public class UserDetail extends User {

	@Column(name = "school", length = 100)
	private String school;

	@Column(name = "phone_number", length = 100, nullable = false, unique = true)
	private String phoneNumber;

	@Column(name = "email", length = 100, nullable = false, unique = true)
	private String email;

	@Column(name = "facebook_link", length = 100, nullable = false)
	private String facebookLink;

	@Temporal(TemporalType.DATE)
	@Column(name = "date_of_birth")
	private Date dateOfBirth;

	@Column(name = "current_address", length = 100)
	private String currentAddress;

	@Column(name = "home_town", length = 100)
	private String homeTown;

	@Column(name = "current_working_location", length = 100)
	private String currentWorkingLocation;

	@Column(name = "identify_number", length = 100, unique = true)
	private String identifyNumber;

	@Temporal(TemporalType.DATE)
	@Column(name = "identify_date")
	private Date identifyDate;

	@Column(name = "identify_place", length = 100)
	private String identifyPlace;

	@Column(name = "permanent_address", length = 100)
	private String permanentAddress;

	@Column(name = "bank_name", length = 100)
	private String bankName;

	@Column(name = "account_number", length = 100)
	private String accountNumber;

	@Column(name = "bank_account_name", length = 100)
	private String bankAccountName;

	@Column(name = "tax_number", length = 100)
	private String taxNumber;

	@Column(name = "tax_address", length = 100)
	private String taxAddress;

	@Column(name = "note", length = 500)

	private String note;

}
