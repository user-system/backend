package com.company.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.hibernate.annotations.CreationTimestamp;

import com.company.entity.view.clazz.CurrentLessonSubjectMentorByClass;
import com.company.entity.view.clazz.StudentRateByClass;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Column;
import jakarta.persistence.Converter;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "`Class`")
@Data
@NoArgsConstructor
public class Clazz implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name = "id")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "`name`", length = 100, nullable = false, unique = true)
	private String name;

	@Column(name = "`code`", length = 100, nullable = false, unique = true)
	private String code;

	@Column(name = "`type`", length = 100, nullable = false)
	private String type;

	@Column(name = "address", length = 100, nullable = false)
	private String address;

	@Column(name = "facebook_link", length = 100, nullable = false)
	private String facebookLink;

	@Column(name = "group_chat", length = 100, nullable = false)
	private String groupChat;

	@Temporal(TemporalType.DATE)
	@Column(name = "start_date", nullable = false)
	private Date startDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "end_date")
	private Date endDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "expected_end_date", nullable = false)
	private Date expectedEndDate;

	@Column(name = "status", nullable = false)
	private ClassStatus status;

	@Column(name = "note", length = 500)
	private String note;

	@Column(name = "created_date")
	@Temporal(TemporalType.TIMESTAMP)
	@CreationTimestamp
	private Date createdDate;

	@Column(name = "modified_date")
	@Temporal(TemporalType.TIMESTAMP)
	@CreationTimestamp
	private Date modifiedDate;
	
	@ManyToOne
	@JoinColumn(name = "course_id", nullable = false)
	private Course course;

	@OneToOne(mappedBy = "clazz", fetch = FetchType.LAZY)
	private CurrentLessonSubjectMentorByClass currentLessonSubjectMentor;

	@OneToMany(mappedBy = "clazz")
	private List<ClassClassSchedule> classSchedules;

	@OneToOne(mappedBy = "clazz", fetch = FetchType.LAZY)
	private StudentRateByClass studentRate;

	@NoArgsConstructor
	@Getter
	public enum ClassStatus {
		NOT_STARTED("Not-started"), ACTIVE("Active"), MERGE("Merge"), FINISHED("Finished");

		private String values;

		private ClassStatus(String values) {
			this.values = values;
		}

		public static ClassStatus toEnum(String sqlStatus) {
			for (ClassStatus item : ClassStatus.values()) {
				if (item.getValues().equals(sqlStatus)) {
					return item;
				}
			}
			return null;
		}
	}
	
}

@Converter(autoApply = true)
class ClassConverter implements AttributeConverter<Clazz.ClassStatus, String> {

	@Override
	public String convertToDatabaseColumn(Clazz.ClassStatus name) {
		if (name == null) {

			return null;
		}
		return name.getValues();
	}

	@Override
	public Clazz.ClassStatus convertToEntityAttribute(String sqlName) {
		if (sqlName == null) {
			return null;
		}
		return Clazz.ClassStatus.toEnum(sqlName);
	}

}