package com.company.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import jakarta.persistence.*;
import org.hibernate.annotations.CreationTimestamp;
import com.company.entity.view.TotalLessonInSubject;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "`subject`")
@Data
@NoArgsConstructor
public class Subject implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name = "id")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "`name`", length = 100, nullable = false)
	private String name;

	@Column(name = "`code`", length = 100, unique = true, nullable = false)
	private String code;

	@Column(name = "`version`", length = 100)
	private String version;

	@Column(name = "`created_date`")
	@Temporal(TemporalType.TIMESTAMP)
	@CreationTimestamp
	private Date createdDate;

	@Column(name = "`modified_date`")
	@Temporal(TemporalType.TIMESTAMP)
	@CreationTimestamp
	private Date modifiedDate;

	@Column(name = "`note`", length = 500)
	private String note;

	@OneToMany(mappedBy = "subject")
	private List<Lesson> lessons;

	@OneToOne(mappedBy = "subject", fetch = FetchType.LAZY)
	private TotalLessonInSubject totalLesson;

	@ManyToOne
	@JoinColumn(name = "creator_id", nullable = false)
	private User creator;

	@ManyToOne
	@JoinColumn(name = "editor_id", nullable = false)
	private User editor;
}
