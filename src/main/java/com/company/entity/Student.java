package com.company.entity;

import java.util.List;

import org.hibernate.annotations.DynamicInsert;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Column;
import jakarta.persistence.Convert;
import jakarta.persistence.Converter;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PrimaryKeyJoinColumn;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "Student")
@PrimaryKeyJoinColumn(name = "id")
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@DynamicInsert

public class Student extends UserDetail {

	@Column(name = "`code`", length = 100, nullable = false, unique = true)
	private String code;

	@OneToMany(mappedBy = "student")
	private List<ClassStudent> classStudent;

	@Column(name = "status", nullable = false)
	@Convert(converter = StudentStatusConverter.class)
	private StudentStatus status;

	@NoArgsConstructor
	@Getter
	public enum StudentStatus {
		NOT_STARTED("Not-started"), ACTIVE("Active"), PENDING("Pending"), DEFERRED("Deferred"), DROP_OUT("Drop-out"),
		FINISHED("Finished");

		private String values;

		private StudentStatus(String values) {
			this.values = values;
		}

		public static StudentStatus toEnum(String sqlStatus) {
			for (StudentStatus item : StudentStatus.values()) {
				if (item.getValues().equals(sqlStatus)) {
					return item;
				}
			}
			return null;
		}
	}

	@PrePersist
	public void setDefault() {
		if (status == null) {
			status = StudentStatus.NOT_STARTED;
		}
	}

}

@Converter(autoApply = true)
class StudentStatusConverter implements AttributeConverter<Student.StudentStatus, String> {

	@Override
	public String convertToDatabaseColumn(Student.StudentStatus name) {
		if (name == null) {

			return null;
		}
		return name.getValues();
	}

	@Override
	public Student.StudentStatus convertToEntityAttribute(String sqlName) {
		if (sqlName == null) {
			return null;
		}
		return Student.StudentStatus.toEnum(sqlName);
	}

}