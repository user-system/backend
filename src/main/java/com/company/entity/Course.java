package com.company.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.hibernate.annotations.CreationTimestamp;

import com.company.entity.view.CourseSubject;
import com.company.entity.view.TotalSubjectTotalLessonInCourse;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "`course`")
@Data
@NoArgsConstructor
public class Course implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name = "`id`")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "`class_code`", length = 100, nullable = false)
	private String classCode;

	@Column(name = "`student_code`", length = 100, nullable = false)
	private String studentCode;

	@Column(name = "`name`", length = 100, nullable = false, unique = true)
	private String name;

	@Column(name = "`standard_tuition`", nullable = false)
	private Integer standardTuition;

	@Column(name = "`hour`", nullable = false)
	private float hour;

	@Column(name = "`created_date`")
	@Temporal(TemporalType.TIMESTAMP)
	@CreationTimestamp
	private Date createdDate;

	@Column(name = "`modified_date`")
	@Temporal(TemporalType.TIMESTAMP)
	@CreationTimestamp
	private Date modifiedDate;

	@Column(name = "`note`", length = 500)
	private String note;

	@ManyToOne
	@JoinColumn(name = "creator_id", nullable = false)
	private User creator;

	@ManyToOne
	@JoinColumn(name = "editor_id", nullable = false)
	private User editor;

	@OneToMany(mappedBy = "course", fetch = FetchType.LAZY)
	private List<CourseSubject> coursesubject;

	@OneToOne(mappedBy = "course")
	private TotalSubjectTotalLessonInCourse totalSubjectTotalLesson;

}
