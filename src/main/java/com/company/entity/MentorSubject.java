package com.company.entity;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.SuperBuilder;

@Entity
@Table(name = "mentor_subject")
@Data
@SuperBuilder
@NoArgsConstructor
public class MentorSubject implements Serializable {
	private static final long serialVersionUID = 1L;
	@EmbeddedId
	@NonNull
	private MentorSubjectPK id;

	@ManyToOne
	@JoinColumn(name = "mentor_id",insertable=false, updatable=false)
	private Mentor mentor;

	@ManyToOne
	@JoinColumn(name = "subject_id",insertable=false, updatable=false)
	private Subject subject;

	@Data
	@NoArgsConstructor
	@Embeddable
	@RequiredArgsConstructor
	public static class MentorSubjectPK implements Serializable {

		private static final long serialVersionUID = 1L;

		@Column(name = "mentor_id")
		@NonNull
		private Integer mentorId;

		@Column(name = "subject_id")
		@NonNull
		private Integer subjectId;

	}
}
