package com.company.entity;

import java.io.Serializable;
import java.util.List;

import org.hibernate.annotations.DynamicInsert;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "Lesson")
@Data
@NoArgsConstructor
@DynamicInsert
public class Lesson implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name = "id")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "`name`", length = 100, nullable = false)
	private String name;

	@Column(name = "`type`", length = 100, nullable = false)
	private String type;
	
	@Column(name = "ordinal", nullable = false)
	private Integer ordinal;

	@ManyToOne
	@JoinColumn(name = "subject_id", nullable = false)
	private Subject subject;

	@OneToMany(mappedBy = "lesson")
	private List<LessonSchedule> schedules;

}
