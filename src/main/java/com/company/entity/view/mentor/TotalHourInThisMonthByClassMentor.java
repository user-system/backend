package com.company.entity.view.mentor;

import java.io.Serializable;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.Immutable;

import com.company.entity.Clazz;
import com.company.entity.Mentor;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;

@Immutable
@Entity
@Table(name = "Total_Hour_In_This_Month_By_Class_Mentor")
@Data
@NoArgsConstructor
public class TotalHourInThisMonthByClassMentor implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@GeneratedValue
	private Integer id;

	@Column(name = "hour_in_month", nullable = false)
	private Float totalHour;

	@OneToOne
	@JoinColumn(name = "mentor_id", nullable = false)
	private Mentor mentor;

	@ManyToOne
	@JoinColumn(name = "class_id", nullable = false)
	private Clazz clazz;
}
