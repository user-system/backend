package com.company.entity.view.mentor;

import java.io.Serializable;

import org.hibernate.annotations.Immutable;

import com.company.entity.Clazz;
import com.company.entity.Lesson;
import com.company.entity.Mentor;
import com.company.entity.Subject;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;

@Immutable
@Entity
@Table(name = "Current_Lesson_By_Subject_Class_Mentor")
@Data
@NoArgsConstructor
public class CurrentLessonBySubjectClassMentor implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@GeneratedValue
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "mentor_id", nullable = false)
	private Mentor mentor;

	@ManyToOne
	@JoinColumn(name = "class_id", nullable = false)
	private Clazz clazz;

	@ManyToOne
	@JoinColumn(name = "subject_id", nullable = false)
	private Subject subject;

	@ManyToOne
	@JoinColumn(name = "current_lesson_id")
	private Lesson currentLesson;
}
