package com.company.entity.view;

import java.io.Serializable;

import com.company.entity.Course;
import com.company.entity.Subject;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.MapsId;
import jakarta.persistence.Table;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "Course_Subject")
@NoArgsConstructor
public class CourseSubject implements Serializable {

	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private CourseSubjectKey id;

	@ManyToOne
	@MapsId("course_id")
	@JoinColumn(name = "course_id")
	private Course course;

	@ManyToOne
	@MapsId("subject_id")
	@JoinColumn(name = "subject_id")
	private Subject subject;

	@Embeddable
	@NoArgsConstructor
	public static class CourseSubjectKey implements Serializable {

		private static final long serialVersionUID = 1L;

		@Column(name = "course_id", nullable = false)
		private int courseId;

		@Column(name = "subject_id", nullable = false)
		private int subjectId;
	}
}
