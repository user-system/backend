package com.company.entity.view;

import java.io.Serializable;

import org.hibernate.annotations.Immutable;

import com.company.entity.Subject;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;

@Immutable
@Entity
@Table(name = "Total_Lesson_In_Subject")
@Data
@NoArgsConstructor
public class TotalLessonInSubject implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name = "`id`")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@OneToOne
	@JoinColumn(name = "`subject_id`")
	private Subject subject;

	@Column(name = "`total_lesson`", nullable = false)
	private Integer totalLesson; 

}
