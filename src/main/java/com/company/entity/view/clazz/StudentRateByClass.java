package com.company.entity.view.clazz;

import java.io.Serializable;

import org.hibernate.annotations.Immutable;

import com.company.entity.Clazz;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;

@Immutable
@Entity
@Table(name = "Student_Rate_By_Class")
@Data
@NoArgsConstructor
public class StudentRateByClass implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@GeneratedValue
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "class_id", nullable = false)
	private Clazz clazz;

	@Column(name = "student_total", nullable = false)
	private Integer studentTotal;

	@Column(name = "not_started_student_total", nullable = false)
	private Integer notStartedStudentTotal;

	@Column(name = "active_student_total", nullable = false)
	private Integer activeStudentTotal;

	@Column(name = "finished_student_total", nullable = false)
	private Integer finishedStudentTotal;

	@Column(name = "move_to_student_total", nullable = false)
	private Integer moveToStudentTotal;

	@Column(name = "dropout_student_total", nullable = false)
	private Integer dropoutStudentTotal;

	@Column(name = "deferred_student_total", nullable = false)
	private Integer deferredStudentTotal;

	@Column(name = "not_started_rate", nullable = false)
	private Float notStartedRate;

	@Column(name = "active_rate", nullable = false)
	private Float activeRate;

	@Column(name = "finished_rate", nullable = false)
	private Float finishedRate;

	@Column(name = "move_to_rate", nullable = false)
	private Float moveToRate;

	@Column(name = "dropout_rate", nullable = false)
	private Float dropoutRate;

	@Column(name = "deferred_rate", nullable = false)
	private Float deferredRate;

}

