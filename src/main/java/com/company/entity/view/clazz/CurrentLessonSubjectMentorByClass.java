package com.company.entity.view.clazz;

import java.io.Serializable;

import com.company.entity.Clazz;
import com.company.entity.Lesson;
import com.company.entity.Mentor;
import com.company.entity.Subject;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "Current_Lesson_Subject_Mentor_By_Class")
@Data
@NoArgsConstructor
public class CurrentLessonSubjectMentorByClass implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Column(name = "id")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "class_id", nullable = false)
	private Clazz clazz;

	@ManyToOne
	@JoinColumn(name = "subject_id", nullable = false)
	private Subject subject;

	@ManyToOne
	@JoinColumn(name = "lesson_id", nullable = false)
	private Lesson lesson;

	@ManyToOne
	@JoinColumn(name = "mentor_id")
	private Mentor mentor;
}

