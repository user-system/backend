package com.company.entity.view;

import java.io.Serializable;

import org.hibernate.annotations.Immutable;

import com.company.entity.Course;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;

@Immutable
@Entity
@Table(name = "Total_Subject_Total_Lesson_In_Course")
@Data
@NoArgsConstructor
public class TotalSubjectTotalLessonInCourse implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name = "`id`")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "`total_lesson`", nullable = false)
	private Integer totalLesson; 

	@OneToOne
	@JoinColumn(name = "`course_id`")
	private Course course;

	@Column(name = "`total_subject`", nullable = false)
	private Integer totalSubject; 

}