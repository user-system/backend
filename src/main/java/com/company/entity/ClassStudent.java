package com.company.entity;

import java.io.Serializable;
import java.util.Date;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.SuperBuilder;

@Entity
@Table(name = "Class_Student")
@Data
@NoArgsConstructor
@SuperBuilder
@DynamicInsert

public class ClassStudent implements Serializable {
	private static final long serialVersionUID = 1L;
	@EmbeddedId
	@NonNull
	private ClassStudentPK id;

	@Enumerated(EnumType.ORDINAL)
	private Status status;

	@ManyToOne
	@JoinColumn(name = "class_id", nullable = false, insertable = false, updatable = false)
	private Clazz clazz;

	@ManyToOne
	@JoinColumn(name = "student_id", nullable = false, insertable = false, updatable = false)
	private Student student;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "joined_date")
	@CreationTimestamp
	private Date joinedDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "end_date")
	private Date endDate;

	@Column(name = "note", length = 500)
	private String note;

	public enum Status {
		NotStarted("Not-started"), Active("Active"), Finished("Finished"), MoveTo("MoveTo"), Deferred("Deferred"),
		Dropout("Dropout");

		public String value;

		private Status(String value) {
			this.value = value;
		}

		@Override
		public String toString() {
			return value;
		}
	}

	@Data
	@NoArgsConstructor
	@Embeddable
	@RequiredArgsConstructor
	public static class ClassStudentPK implements Serializable {

		private static final long serialVersionUID = 1L;

		@Column(name = "class_id")
		@NonNull
		private Integer classId;

		@Column(name = "student_id")
		@NonNull
		private Integer studentId;

	}

}