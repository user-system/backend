package com.company.entity;

import java.io.Serializable;

import org.hibernate.annotations.DynamicInsert;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.MapsId;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.SuperBuilder;

@Entity
@Table(name = "Class_Class_Schedule")
@Data
@SuperBuilder
@NoArgsConstructor
@RequiredArgsConstructor
@DynamicInsert
public class ClassClassSchedule implements Serializable {

	private static final long serialVersionUID = 1L;

	@EmbeddedId
	@NonNull
	private ClassClassSchedulePK id;

	@ManyToOne
	@MapsId("class_schedule_id")
	@JoinColumn(name = "class_schedule_id", nullable = false)
	@NonNull
	private ClassSchedule schedule;

	@ManyToOne
	@MapsId("class_id")
	@JoinColumn(name = "class_id", nullable = false)
	@NonNull
	private Clazz clazz;

	@Embeddable
	@Data
	@NoArgsConstructor
	@RequiredArgsConstructor
	public static class ClassClassSchedulePK implements Serializable {

		private static final long serialVersionUID = 1L;

		@Column(name = "class_schedule_id", nullable = false)
		@NonNull
		private Integer scheduleId;

		@Column(name = "class_id", nullable = false)
		@NonNull
		private Integer classId;
	}

}
