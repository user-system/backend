package com.company.entity;

import java.io.Serializable;
import java.util.Date;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.Formula;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Inheritance;
import jakarta.persistence.InheritanceType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import lombok.Data;
import lombok.NoArgsConstructor;

import lombok.experimental.SuperBuilder;

@Entity
@Table(name = "`User`")
@Inheritance(strategy = InheritanceType.JOINED)
@Data
@SuperBuilder
@NoArgsConstructor
@DynamicInsert

public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name = "id")
	@Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Integer id;

	@Column(name = "username", length = 100, nullable = false)
	private String username;

	@Formula("concat(first_name,' ',last_name)")
	private String fullname;

	@Column(name = "first_name", length = 100, nullable = false)
	private String firstname;

	@Column(name = "last_name", length = 100, nullable = false)
	private String lastname;

	@Column(name = "gender", length = 100, nullable = false)
	private String gender;

	@Column(name = "`created_date`")
	@Temporal(TemporalType.TIMESTAMP)
	@CreationTimestamp
	private Date createdDate;

	@Column(name = "`modified_date`")
	@Temporal(TemporalType.TIMESTAMP)
	@CreationTimestamp
	private Date modifiedDate;

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "editor_id", nullable = false)
	private User editor;

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "creator_id", nullable = false)
	private User creator;

	@Column(name = "`role`")
	private String role;

}

