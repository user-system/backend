package com.company.entity;

import java.util.List;

import com.company.entity.view.mentor.CurrentLessonBySubjectClassMentor;
import com.company.entity.view.mentor.TotalHourInThisMonthByMentor;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Column;
import jakarta.persistence.Converter;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PrimaryKeyJoinColumn;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Entity
@Table(name = "Mentor")
@PrimaryKeyJoinColumn(name = "id")
@Data

@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Mentor extends UserDetail {
	private static final long serialVersionUID = 1L;

	@Column(name = "status", nullable = false)
	private MentorStatus status;

	@Column(name = "type", length = 100)
	private String type;

	@OneToMany(mappedBy = "mentor")
	private List<MentorSubject> teachableSubjects;

	@OneToMany(mappedBy = "mentor")
	private List<ClassMentor> classMentors;

	@OneToMany(mappedBy = "mentor")
	private List<CurrentLessonBySubjectClassMentor> currentLessonByClasses;

	@OneToOne(mappedBy = "mentor", fetch = FetchType.LAZY)
	private TotalHourInThisMonthByMentor totalHourInThisMonth;
	@NoArgsConstructor
	@Getter
	public enum MentorStatus {
		ACTIVE("Active"), INACTIVE("Inactive");

		private String value;

		private MentorStatus(String value) {
			this.value = value;
		}

		public static MentorStatus toEnum(String sqlStatus) {
			for (MentorStatus item : MentorStatus.values()) {
				if (item.getValue().equals(sqlStatus)) {
					return item;
				}
			}
			return null;
		}
	}

	@PrePersist
	public void setDefault() {
		if (status == null) {
			status = MentorStatus.ACTIVE;
		}
	}
}

@Converter(autoApply = true)
class MentorRoleConverter implements AttributeConverter<Mentor.MentorStatus, String> {

	public String convertToDatabaseColumn(Mentor.MentorStatus name) {
		if (name == null) {
			return null;
		}
		return name.getValue();
	}

	public Mentor.MentorStatus convertToEntityAttribute(String sqlName) {
		if (sqlName == null) {
			return null;
		}
		return Mentor.MentorStatus.toEnum(sqlName);
	}
}
