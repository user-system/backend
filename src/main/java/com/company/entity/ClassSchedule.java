package com.company.entity;

import java.io.Serializable;
import java.util.List;

import org.hibernate.annotations.DynamicInsert;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Entity
@Table(name = "Class_Schedule")
@Data
@SuperBuilder
@NoArgsConstructor
@DynamicInsert
public class ClassSchedule implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name = "id")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "`name`", length = 100, nullable = false)
	private String name;

	@OneToMany(mappedBy = "schedule")
	private List<ClassClassSchedule> classSchedules;
}