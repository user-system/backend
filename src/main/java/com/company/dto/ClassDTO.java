package com.company.dto;

import java.util.List;

import com.company.entity.Clazz.ClassStatus;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ClassDTO {

	private Integer id;

	private String name;

	private String code;

	private String address;

	private ClassStatus status;

	@JsonProperty("schedules")
	private List<ScheduleDto> classSchedules;

	private studentRate studentRate;

	private currentLessonSubjectMentor currentLessonSubjectMentor;

	@Data
	@NoArgsConstructor
	static class currentLessonSubjectMentor {
		private String lessonName;
		private String subjectName;
		private String mentorFullname;
	}

	@Data
	@NoArgsConstructor
	static class ScheduleDto {
		@JsonProperty("name")
		private String scheduleName;
	}

	@Data
	@NoArgsConstructor
	static class MentorDto {
		private String fullName;
		private String username;
	}

	@Data
	@NoArgsConstructor
	static class studentRate {
		private Float dropoutRate;
		private Float deferredRate;
		private Float pendingRate;
		private Float activeRate;
		private Float finishedRate;
		private Float moveToRate;
	}
}
