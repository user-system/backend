package com.company.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CourseDTO {

	private Integer id;

	private String classCode;

	private String studentCode;

	private String name;

	private int standardTuition;

	private float hour;

	private Date createdDate;
	
	private String creatorFullName;
	
	private String creatorId;


	
	@JsonProperty("TotalLesson")
	private Integer totalSubjectTotalLessonTotalLesson;
	
	@JsonProperty("TotalSubject")
	private Integer totalSubjectTotalLessonTotalSubject;

}