package com.company.dto;

import java.util.Date;
import java.util.List;

import com.company.entity.Clazz;
import com.fasterxml.jackson.annotation.JsonProperty;

import ch.qos.logback.core.status.Status;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ClassStudentDTO {

	private Status status;

	@JsonProperty("fullName")
	private String studentFullName;

	@JsonProperty("username")
	private String studentUsername;

	private Date joinedDate;

	private Date endDate;

	@JsonProperty("Clazz")
	private List<Clazz> clazzName;

}
