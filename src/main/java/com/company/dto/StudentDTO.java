package com.company.dto;

import java.util.List;

import com.company.entity.Student.StudentStatus;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class StudentDTO {
	private String code;
	private String username;
	private String fullname;
	private String phoneNumber;
	private String email;
	private StudentStatus status;
	@JsonProperty("classes")
	private List<ClassStudentDTO> classStudent;

	@Data
	@NoArgsConstructor
	static class ClassStudentDTO {
		@JsonProperty("name")
		private String clazzName;
	}

}
