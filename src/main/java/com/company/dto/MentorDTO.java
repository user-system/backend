package com.company.dto;

import java.util.Date;
import java.util.List;

import com.company.entity.Mentor.MentorStatus;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class MentorDTO {

	private Integer id;
	private String username;
	private String fullname;
	private MentorStatus status;
	private String creatorFullname	;
	private Date createdDate;

	@JsonProperty("totalHourInThisMonth")
	private Float totalHourInThisMonthTotalHour;

	@JsonProperty("teachings")
	private List<CurrentLessonBySubjectClassMentorDto> currentLessonByClasses;

	@Data
	@NoArgsConstructor
	static class CurrentLessonBySubjectClassMentorDto {
		private Integer clazzId;
		private String clazzName;
		private String subjectId;
		private String subjectName;	
		private Integer currentLessonId;
		private String currentLessonName;

		@JsonProperty("totalHour")
		private Float clazzTotalHourInThisMonthByMentorTotalHour;
	}
	
}
