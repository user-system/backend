package com.company.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SubjectDTO {

	private Integer id;

	private String name;

	private String code;

	private String version;

	private String creatorFullName;

	private Integer totalLesson;
}
