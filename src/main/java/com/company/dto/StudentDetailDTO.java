package com.company.dto;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor

public class StudentDetailDTO {

	private String firstname;
	private String lastname;
	private String gender;
	private String username;
	private String code;
	private String phoneNumber;
	private String email;
	private String faceBook;
	private String university;
	private Date dateOfBirth;
	private String currentAddress;
	private String homeTown;
	private String currentWorkingLocation;
	private String classCode;
	private Long standardTuition;
	private String paymentType;
	private Long payableTuition;
	private Long paiedTuition;
	private String status;

}
