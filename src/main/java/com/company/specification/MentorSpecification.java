package com.company.specification;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

import com.company.entity.Mentor;
import com.company.form.mentor.MentorFilterForm;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

public class MentorSpecification {

	@SuppressWarnings("deprecation")
	public static Specification<Mentor> builWhere(MentorFilterForm mentorFilterForm) {
		Specification<Mentor> where = null;

		if (mentorFilterForm == null) {
			return where;
		}

		if (!StringUtils.isEmpty(mentorFilterForm.getSearch())) {
			String search = mentorFilterForm.getSearch();
			search = search.trim();
			CustomSpecification fullname = new CustomSpecification("fullname", search);
			CustomSpecification creatorFullname = new CustomSpecification("creatorFullname", search);
			where = Specification.where(fullname).or(creatorFullname);
		}
		if (mentorFilterForm.getStatus() != null) {
			CustomSpecification statusSpecification = new CustomSpecification("status", mentorFilterForm.getStatus());
			if (where == null) {
				where = statusSpecification;
			} else {
				where = where.and(statusSpecification);
			}
            
        }

		return where;
	}

	@SuppressWarnings("serial")
	@RequiredArgsConstructor
	static class CustomSpecification implements Specification<Mentor> {

		@NonNull
		private String field;
		@NonNull
		private Object value;

		@Override
		public Predicate toPredicate(Root<Mentor> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {

			if (field.equalsIgnoreCase("fullname")) {
				return criteriaBuilder.like(root.get("fullname"), "%" + value.toString() + "%");
			}
			if (field.equalsIgnoreCase("creatorFullname")) {
				return criteriaBuilder.like(root.get("creator").get("fullname"), "%" + value.toString() + "%");
			}
			if (field.equalsIgnoreCase("status")) {
				return criteriaBuilder.equal(root.get("status"), value);
			}

			return null;
		}

	}

}
