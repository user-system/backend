package com.company.specification;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;
import com.company.entity.Subject;
import com.company.form.subject.SubjectFilterForm;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

public class SubjectSpecification {

	@SuppressWarnings("deprecation")
	public static Specification<Subject> buildWhere(SubjectFilterForm subjectFilterForm) {

		Specification<Subject> where = null;

		if (subjectFilterForm == null) {
			return where;
		}
		
		if (!StringUtils.isEmpty(subjectFilterForm.getSearch())) {
			String search = subjectFilterForm.getSearch();
			search = search.trim();
			CustomSpecification name = new CustomSpecification("name", search);
			CustomSpecification version = new CustomSpecification("version", search);
			CustomSpecification code = new CustomSpecification("code", search);
			CustomSpecification creator = new CustomSpecification("creator", search);
			where = Specification.where(name).or(version).or(code).or(creator);

		}
		return where;
	}
	@SuppressWarnings("serial")
	@RequiredArgsConstructor
	static class CustomSpecification implements Specification<Subject> {

		@NonNull
		private String field;
		@NonNull
		private Object value;

		@Override
		public Predicate toPredicate(Root<Subject> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {

			if (field.equalsIgnoreCase("name")) {
				return criteriaBuilder.like(root.get("name"), "%" + value.toString() + "%");
			} else if (field.equalsIgnoreCase("version")) {
				return criteriaBuilder.like(root.get("version"), "%" + value.toString() + "%");
			}else if (field.equalsIgnoreCase("code")) {
				return criteriaBuilder.like(root.get("code"), "%" + value.toString() + "%");
			}else if (field.equalsIgnoreCase("creator")) {
				return criteriaBuilder.like(root.get("creator").get("fullName"), "%" + value.toString() + "%" );
			}
			return null;
		}
	}
}

