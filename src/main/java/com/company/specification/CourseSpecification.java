package com.company.specification;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

import com.company.entity.Course;
import com.company.form.course.CourseFilterForm;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

public class CourseSpecification {
	@SuppressWarnings("deprecation")
	public static Specification<Course> builWhere(CourseFilterForm courseFilterForm) {

		Specification<Course> where = null;

		if (courseFilterForm == null) {
			return where;
		}

		if (!StringUtils.isEmpty(courseFilterForm.getSearch())) {
			String search = courseFilterForm.getSearch();
			search = search.trim();
			CustomSpecification name = new CustomSpecification("name", search);
			CustomSpecification creator = new CustomSpecification("creator", search);

			where = Specification.where(name).or(creator);
		}
		;

		return where;

	}

	@SuppressWarnings("serial")
	@RequiredArgsConstructor
	static class CustomSpecification implements Specification<Course> {

		@NonNull
		private String field;
		@NonNull
		private Object value;

		@Override
		public Predicate toPredicate(Root<Course> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {

			if (field.equalsIgnoreCase("name")) {
				return criteriaBuilder.like(root.get("name"), "%" + value.toString() + "%");
			} else if (field.equalsIgnoreCase("creator")) {
				return criteriaBuilder.like(root.get("creator").get("fullname"), "%" + value.toString() + "%");
			}

			return null;
		}

	}
}
