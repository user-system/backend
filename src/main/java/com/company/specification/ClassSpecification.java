package com.company.specification;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

import com.company.entity.Clazz;
import com.company.form.clazz.ClassFilterForm;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

public class ClassSpecification {

	@SuppressWarnings("deprecation")
	public static Specification<Clazz> builWhere(ClassFilterForm classFilterForm) {

		Specification<Clazz> where = null;

		if (classFilterForm == null) {
			return where;
		}

		if (!StringUtils.isEmpty(classFilterForm.getSearch())) {
			String search = classFilterForm.getSearch();
			search = search.trim();
			CustomSpecification name = new CustomSpecification("name", search);
			CustomSpecification code = new CustomSpecification("code", search);
			CustomSpecification address = new CustomSpecification("address", search);
			
			where = Specification.where(name).or(code).or(address);
		}

		if (classFilterForm.getStatus() != null) {
			CustomSpecification status = new CustomSpecification("status", classFilterForm.getStatus());
			if (where == null) {
				where = status;
			} else {
				where = status.and(where);
			}
		}
		return where;
	}

	@SuppressWarnings("serial")
	@RequiredArgsConstructor
	static class CustomSpecification implements Specification<Clazz> {

		@NonNull
		private String field;
		@NonNull
		private Object value;

		@Override
		public Predicate toPredicate(Root<Clazz> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {

			if (field.equalsIgnoreCase("name")) {
				return criteriaBuilder.like(root.get("name"), "%" + value.toString() + "%");
			}
			if (field.equalsIgnoreCase("code")) {
				return criteriaBuilder.like(root.get("code"), "%" + value.toString() + "%");
			}
			if (field.equalsIgnoreCase("address")) {
				return criteriaBuilder.like(root.get("address"), "%" + value.toString() + "%");
			}
			if (field.equalsIgnoreCase("status")) {
				return criteriaBuilder.equal(root.get("status"), value);
			}

			return null;
		}

	}
}
