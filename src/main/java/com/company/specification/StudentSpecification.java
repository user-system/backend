package com.company.specification;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

import com.company.entity.Student;
import com.company.form.student.StudentFilterForm;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

public class StudentSpecification {

	@SuppressWarnings("deprecation")
	public static Specification<Student> builWhere(StudentFilterForm studentFilterForm) {

		Specification<Student> where = null;

		if (studentFilterForm == null) {
			return where;
		}

		if (!StringUtils.isEmpty(studentFilterForm.getSearch())) {
			String search = studentFilterForm.getSearch();
			search = search.trim();
			CustomSpecification username = new CustomSpecification("username", search);
			CustomSpecification code = new CustomSpecification("code", search);
			CustomSpecification fullname = new CustomSpecification("fullname", search);
			CustomSpecification phonenumber = new CustomSpecification("phoneNumber", search);
			CustomSpecification email = new CustomSpecification("email", search);

			where = Specification.where(username).or(code).or(fullname).or(phonenumber).or(email);
		}

		if (studentFilterForm.getStatus() != null) {
			CustomSpecification status = new CustomSpecification("status", studentFilterForm.getStatus());
			if (where == null) {
				where = status;
			} else {
				where = status.and(where);
			}
		}

		return where;

	}

	@SuppressWarnings("serial")
	@RequiredArgsConstructor
	static class CustomSpecification implements Specification<Student> {

		@NonNull
		private String field;
		@NonNull
		private Object value;

		@Override
		public Predicate toPredicate(Root<Student> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {

			if (field.equalsIgnoreCase("username")) {
				return criteriaBuilder.like(root.get("username"), "%" + value.toString() + "%");
			}
			if (field.equalsIgnoreCase("code")) {
				return criteriaBuilder.like(root.get("code"), "%" + value.toString() + "%");
			}
			if (field.equalsIgnoreCase("fullname")) {
				return criteriaBuilder.like(root.get("fullname"), "%" + value.toString() + "%");
			}
			
			if (field.equalsIgnoreCase("phoneNumber")) {
				return criteriaBuilder.like(root.get("phoneNumber"), "%" + value.toString() + "%");
			}

			if (field.equalsIgnoreCase("email")) {
				return criteriaBuilder.like(root.get("email"), "%" + value.toString() + "%");
			}

			if (field.equalsIgnoreCase("status")) {
				return criteriaBuilder.equal(root.get("status"), value);
			}

			return null;
		}

	}
}
