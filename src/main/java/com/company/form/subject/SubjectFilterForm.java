package com.company.form.subject;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

@Data
@NoArgsConstructor
public class SubjectFilterForm {
	@Length(max = 50, message = "The search length is max 50 characters")
	private String search;
}
