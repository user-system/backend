package com.company.form.course;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CourseFilterForm {
	private String search;

}
