package com.company.form.student;

import com.company.entity.Student.StudentStatus;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class StudentFilterForm {

	private String search;
	private StudentStatus status;

}
