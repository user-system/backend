package com.company.form.mentor;

import com.company.entity.Mentor.MentorStatus;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class MentorFilterForm {
	private String search;

	private MentorStatus status;
}
