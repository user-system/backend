package com.company.form.clazz;

import com.company.entity.Clazz.ClassStatus;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ClassFilterForm {
	
	private String search;	
	
	private ClassStatus status;
}
