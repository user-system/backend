package com.company.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.company.dto.ClassDTO;
import com.company.entity.Clazz;
import com.company.form.clazz.ClassFilterForm;
import com.company.service.ClassService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("api/v1/classes")
@Validated
public class ClazzController extends BaseController{

	@Autowired
	private ClassService classService;
	
	@GetMapping
	public Page<ClassDTO> getAllClasses(Pageable pageable,@Valid ClassFilterForm classFilterForm) {
		Page<Clazz> entityPage = classService.getAllClasses(pageable, classFilterForm);
		return convertEntityPageToDtoPage(entityPage, pageable, ClassDTO.class);
	}
		
	@GetMapping("/{id}")
	public ClassDTO getClassById(@PathVariable(name = "id") Integer id) {
		Clazz clazz = classService.getClassById(id);
		return convertEntityToDto(clazz, ClassDTO.class);
	}
}

