package com.company.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.company.dto.MentorDTO;
import com.company.entity.Mentor;
import com.company.form.mentor.MentorFilterForm;
import com.company.service.MentorService;

@RestController
@RequestMapping("api/v1/mentors")
@Validated
public class MentorController extends BaseController{
	@Autowired
	private MentorService mentorService;
	
	@GetMapping
	public Page<MentorDTO> getAllMentors(Pageable pageable,MentorFilterForm mentorFilterForm) {
		Page<Mentor> entityPage = mentorService.getAllMentors(pageable, mentorFilterForm);
		return convertEntityPageToDtoPage(entityPage, pageable, MentorDTO.class);
	}

}
