package com.company.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.company.dto.CourseDTO;
import com.company.entity.Course;
import com.company.form.course.CourseFilterForm;
import com.company.service.CourseService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("api/v1/courses")
public class CourseController extends BaseController {

	@Autowired
	private CourseService courseService;

	@GetMapping
	public Page<CourseDTO> getAllCourses(Pageable pageable,@Valid CourseFilterForm courseFilterForm) {
		Page<Course> entityPage = courseService.getAllCourses(pageable, courseFilterForm);
		return convertEntityPageToDtoPage(entityPage, pageable, CourseDTO.class);
	}

	@GetMapping("/{id}")
	public CourseDTO getCourseById(@PathVariable(name = "id") Integer id) {
		Course course = courseService.getCourseById(id);
		return convertEntityToDto(course, CourseDTO.class);
	}
}
