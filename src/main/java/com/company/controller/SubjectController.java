package com.company.controller;

import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.company.dto.SubjectDTO;
import com.company.entity.Subject;
import com.company.form.subject.SubjectFilterForm;
import com.company.service.SubjectService;

@RestController
@RequestMapping("api/v1/subjects")
@Validated
public class SubjectController extends BaseController {

	@Autowired
	private SubjectService subjectService;

	@GetMapping
	public Page<SubjectDTO> getAllSubjects(Pageable pageable, @Valid SubjectFilterForm subjectFilterForm) {
		Page<Subject> entityPage = subjectService.getAllSubjects(pageable, subjectFilterForm);
		return convertEntityPageToDtoPage(entityPage, pageable, SubjectDTO.class);
	}

	@GetMapping("/{id}")
	public SubjectDTO getSubjectById(@PathVariable(name = "id") Integer id) {
		Subject subject = subjectService.getSubjectById(id);
		return convertEntityToDto(subject, SubjectDTO.class);
	}

}
