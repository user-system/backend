package com.company.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.company.dto.StudentDTO;
import com.company.entity.Student;
import com.company.form.student.StudentFilterForm;
import com.company.service.StudentService;

@RestController
@RequestMapping(value = "api/v1/students")
public class StudentController extends BaseController {

	@Autowired
	private StudentService service;

	@GetMapping
	public Page<StudentDTO> getAllStudents(Pageable pageable, StudentFilterForm studentFilterForm) {
		Page<Student> entityPage = service.getAllStudents(pageable, studentFilterForm);
		return convertEntityPageToDtoPage(entityPage, pageable, StudentDTO.class);
	}

	@GetMapping("/{id}")
	public StudentDTO getStudentById(@PathVariable(name = "id") Integer id) {
		Student student = service.getStudentById(id);
		return convertEntityToDto(student, StudentDTO.class);
	}

}
