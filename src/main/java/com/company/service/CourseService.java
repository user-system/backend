package com.company.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.company.entity.Course;
import com.company.form.course.CourseFilterForm;

public interface CourseService {

	Page<Course> getAllCourses(Pageable pageable, CourseFilterForm courseFilterForm);

	Course getCourseById(Integer id);

}
