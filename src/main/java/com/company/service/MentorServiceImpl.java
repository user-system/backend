package com.company.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.company.entity.Mentor;
import com.company.form.mentor.MentorFilterForm;
import com.company.repository.IMentorRepository;
import com.company.specification.MentorSpecification;

@Service
@Transactional
public class MentorServiceImpl implements MentorService {
	
	@Autowired
	private IMentorRepository mentorRepository;
	
	@Override
	public Page<Mentor> getAllMentors(Pageable pageable, MentorFilterForm mentorFilterForm) {
		Specification<Mentor> where = MentorSpecification.builWhere(mentorFilterForm);
		return mentorRepository.findAll(where, pageable);
	}
}
