package com.company.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.company.entity.Mentor;
import com.company.form.mentor.MentorFilterForm;

public interface MentorService {

		Page<Mentor> getAllMentors(Pageable pageable, MentorFilterForm mentorFilterForm);

}
