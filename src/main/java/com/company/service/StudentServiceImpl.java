package com.company.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.company.entity.Student;
import com.company.form.student.StudentFilterForm;
import com.company.repository.IStudentRepository;
import com.company.specification.StudentSpecification;

@Service
public class StudentServiceImpl implements StudentService {

	@Autowired
	private IStudentRepository studentRepository;

	@Override
	public Page<Student> getAllStudents(Pageable pageable, StudentFilterForm studentFilterForm) {
		Specification<Student> where = StudentSpecification.builWhere(studentFilterForm);
		return studentRepository.findAll(where, pageable);
	}

	@Override
	public Student getStudentById(int id) {
		return studentRepository.findById(id).get();
	}

}
