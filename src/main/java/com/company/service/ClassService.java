package com.company.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.company.entity.Clazz;
import com.company.form.clazz.ClassFilterForm;

public interface ClassService {
	Page<Clazz> getAllClasses(Pageable pageable, ClassFilterForm classFilterForm);
	
	Clazz getClassById (Integer id);
}
