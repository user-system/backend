package com.company.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.company.entity.Course;
import com.company.form.course.CourseFilterForm;
import com.company.repository.ICourseRepository;
import com.company.specification.CourseSpecification;

@Service
public class CourseServiceImpl implements CourseService {

	@Autowired
	private ICourseRepository courseRepository;

	@Override
	public Page<Course> getAllCourses(Pageable pageable, CourseFilterForm courseFilterForm) {
		Specification<Course> where = CourseSpecification.builWhere(courseFilterForm);
		return courseRepository.findAll(where, pageable);
	}

	@Override
	public Course getCourseById(Integer id) {
		return courseRepository.findById(id).get();
	}
}
