package com.company.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.company.entity.Subject;
import com.company.form.subject.SubjectFilterForm;
public interface SubjectService {

	Page<Subject> getAllSubjects(Pageable pageable, SubjectFilterForm subjectFilterForm);

	Subject getSubjectById(Integer id);

}
