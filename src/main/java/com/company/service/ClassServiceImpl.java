package com.company.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.company.entity.Clazz;
import com.company.form.clazz.ClassFilterForm;
import com.company.repository.IClassRepository;
import com.company.specification.ClassSpecification;

@Service
public class ClassServiceImpl implements ClassService {

	@Autowired
	private IClassRepository classRepository;

	@Override
	public Page<Clazz> getAllClasses(Pageable pageable, ClassFilterForm classFilterForm) {
		Specification<Clazz> where = ClassSpecification.builWhere(classFilterForm);
		return classRepository.findAll(where, pageable);
	}

	@Override
	public Clazz getClassById(Integer id) {
		return classRepository.findById(id).get();
	}
	
}
