package com.company.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.company.entity.Subject;
import com.company.form.subject.SubjectFilterForm;
import com.company.repository.ISubjectRepository;
import com.company.specification.SubjectSpecification;

@Service
public class SubjectServiceImpl implements SubjectService {

	@Autowired
	private ISubjectRepository subjectRepository;

	@Override
	public Page<Subject> getAllSubjects(Pageable pageable, SubjectFilterForm subjectFilterForm) {
		Specification<Subject> where = SubjectSpecification.buildWhere(subjectFilterForm);
		return subjectRepository.findAll(where, pageable);
	}

	@Override
	public Subject getSubjectById(Integer id) {
		return subjectRepository.findById(id).get();
	}


}
