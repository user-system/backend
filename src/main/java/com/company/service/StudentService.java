package com.company.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.company.entity.Student;
import com.company.form.student.StudentFilterForm;

public interface StudentService {

	public Page<Student> getAllStudents(Pageable pageable, StudentFilterForm studentFilterForm);

	public Student getStudentById(int id);
}
