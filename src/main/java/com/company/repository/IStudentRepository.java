package com.company.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.company.entity.Student;

@Repository
public interface IStudentRepository extends JpaRepository<Student, Integer>, JpaSpecificationExecutor<Student> {

	Student findByCode(String code);

	boolean existsByCode(String code);
}
