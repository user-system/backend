package com.company.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.company.entity.Mentor;

public interface IMentorRepository extends JpaRepository<Mentor, Integer>, JpaSpecificationExecutor<Mentor> {

}
