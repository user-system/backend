package com.company.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.company.entity.Subject;


public interface ISubjectRepository extends JpaRepository<Subject, Integer>, JpaSpecificationExecutor<Subject> {
}
