package com.company.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.company.entity.Clazz;

public interface IClassRepository extends JpaRepository<Clazz, Integer>, JpaSpecificationExecutor<Clazz> {

}
