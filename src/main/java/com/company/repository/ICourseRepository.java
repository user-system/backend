package com.company.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.company.entity.Course;

public interface ICourseRepository extends JpaRepository<Course, Integer>, JpaSpecificationExecutor<Course> {

}
