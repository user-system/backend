package com.company.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.company.entity.Lesson;

public interface ILessonRepository extends JpaRepository<Lesson, Integer>, JpaSpecificationExecutor<Lesson> {

}
